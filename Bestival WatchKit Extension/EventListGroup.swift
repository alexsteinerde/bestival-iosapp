//
//  EventListGroup.swift
//  Bestival
//
//  Created by Alex on 12/09/15.
//  Copyright (c) 2015 Alex Steiner. All rights reserved.
//

import UIKit
import WatchKit

class EventListGroup: NSObject {
    
    @IBOutlet weak var titleLabel: WKInterfaceLabel!
    @IBOutlet weak var image: WKInterfaceImage!
}

