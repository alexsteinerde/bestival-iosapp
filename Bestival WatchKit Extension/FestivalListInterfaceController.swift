//
//  FestivalListInterfaceController.swift
//  Bestival
//
//  Created by Alex on 12/09/15.
//  Copyright (c) 2015 Alex Steiner. All rights reserved.
//

import WatchKit
import Foundation
import BestivalKit


class FestivalListInterfaceController: WKInterfaceController, BFestivalDelegate {

    @IBOutlet weak var tableView: WKInterfaceTable!
    private var thisFestival:BFestival?
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        if let festival = context as? BFestival {
            self.thisFestival = festival
            festival.requestActs(self)
            loadTable()
        }
        // Configure interface objects here.
    }
    
    func loadTable() {
        if let festival = thisFestival {
            tableView.setNumberOfRows(festival.acts.count, withRowType: "cell")
            for (index, art) in enumerate(festival.acts) {
                if let row = tableView.rowControllerAtIndex(index) as? EventListGroup {
                    row.titleLabel.setText(art.titleWithOutType())
                    art.artistImage({
                        (image:UIImage?) in
                        row.image.setImage(image)
                    })
                }
            }
        }
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    //MARK: Festival
    func festival(festival: BFestival?) {
        self.thisFestival = festival
        loadTable()
    }
}
