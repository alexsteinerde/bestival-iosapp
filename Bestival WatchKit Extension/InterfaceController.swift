//
//  InterfaceController.swift
//  Bestival WatchKit Extension
//
//  Created by Alex on 12/09/15.
//  Copyright (c) 2015 Alex Steiner. All rights reserved.
//

import WatchKit
import Foundation
import BestivalKit


class InterfaceController: WKInterfaceController, BEventDelegate {
    
    @IBOutlet weak var tableView: WKInterfaceTable!
    
    var events = Array<BEvent>()

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        BEvent.requestSortedEvents(self)
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    func events(events: Array<BEvent>?) {
        if let events = events {
            self.events = events
            tableView.setNumberOfRows(events.count, withRowType: "cell")
            for (index, event) in enumerate(events) {
                if let row = tableView.rowControllerAtIndex(index) as? EventListGroup {
                    row.titleLabel.setText(event.titleWithOutType())
                    if event.type == .Concert {
                        event.artistImage({
                            (image:UIImage?) in
                            row.image.setImage(image)
                        })
                    }
                    else {
                        row.image.setImage(event.type.image())
                    }
                }
            }
        }
    }
    
    override func contextForSegueWithIdentifier(segueIdentifier: String, inTable table: WKInterfaceTable, rowIndex: Int) -> AnyObject? {
        let event = events[rowIndex]
        return event
    }

}
