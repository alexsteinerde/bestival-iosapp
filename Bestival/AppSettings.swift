//
//  AppSettings.swift
//  Bestival
//
//  Created by Alex on 11/09/15.
//  Copyright (c) 2015 Alex Steiner. All rights reserved.
//

import UIKit

let BKVersion = "1.0"
var BKBuild:Int {
    get {
        let path = NSBundle.mainBundle().pathForResource("Info", ofType: "plist")
        let dict = NSDictionary(contentsOfFile: path!)
        if let build = dict?.valueForKey("CFBundleVersion") as? String, buildNumber = build.toInt() {
            return buildNumber
        }
        else {
            return 0
        }
    }
}
let BKPlayer = SpotifyPlayer()
var BKSpotifySession:SPTSession?