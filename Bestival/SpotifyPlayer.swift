//
//  SpotifyPlayer.swift
//  Bestival
//
//  Created by Alex on 12/09/15.
//  Copyright (c) 2015 Alex Steiner. All rights reserved.
//

import Foundation
import BestivalKit

class SpotifyPlayer {
    var spotifyPlayer:SPTAudioStreamingController?
    
    func playSong(act:BAct) {
        if spotifyPlayer == nil {
            spotifyPlayer = SPTAudioStreamingController(clientId: SPTAuth.defaultInstance().clientID)
        }
        if let player = spotifyPlayer {
            if player.loggedIn {
                self.playUri(act)
            }
            player.loginWithSession(BKSpotifySession, callback: {
                (error:NSError!) in
                if error != nil {
                    println("Logging error: \(error.description)")
                    return
                }
                self.playUri(act)
            })
        }
    }
    
    func playUri(act:BAct) {
        if let player = spotifyPlayer {
            player.playURIs(tracksToUris(act.tracks), fromIndex: 0, callback: {
                (error:NSError!) in
                if error != nil {
                    println("Playback error: \(error.description)")
                }
                return
            })
        }
    }
    
    func stop() {
        if let player = spotifyPlayer {
            player.stop(nil)
        }
    }
    
    func isPlaying()->Bool {
        if let player = spotifyPlayer {
            return player.isPlaying
        }
        return false
    }
    
    func playPause() {
        if let player = spotifyPlayer {
            player.setIsPlaying(!player.isPlaying, callback: nil)
        }
    }
    
    func tracksToUris(tracks:Array<BTrack>)->Array<NSURL> {
        var uris = Array<NSURL>()
        for track in tracks {
            uris += [NSURL(string: track.uri)!]
        }
        return uris
    }
}