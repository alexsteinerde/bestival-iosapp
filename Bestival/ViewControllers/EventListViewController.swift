//
//  EventListViewController.swift
//  Bestival
//
//  Created by Alex on 12/09/15.
//  Copyright (c) 2015 Alex Steiner. All rights reserved.
//

import UIKit
import BestivalKit
import EventKit

class EventListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, BEventDelegate, BActTracksDelegate {
    
    // IBActions
    @IBAction func playerButton(sender: AnyObject) {
        let player = BKPlayer
        player.playPause()
        if !player.isPlaying() {
            actionButton.setTitle(NSLocalizedString("Pause", comment: "Pause a song while playing"), forState: .allZeros)
        }
        else {
            actionButton.setTitle(NSLocalizedString("Play", comment: "Play a song while playing"), forState: .allZeros)
        }
    }
    
    // IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    
    // Variables
    var events = Array<BEvent>()
    
    // Main methods
    override func viewDidLoad() {
        super.viewDidLoad()
        BEvent.requestSortedEvents(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let destinationController = segue.destinationViewController as? FestivalListViewController, event = sender as? BFestival where segue.identifier == "showFestivalList" {
            destinationController.festival = event
        }
    }
    
    //MARK: TableView
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let event = events[indexPath.row]
        
        let cell:EventListTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as! EventListTableViewCell
        if event.type == .Concert {
            event.artistImage({
                (image:UIImage?) in
                cell.iconImage.image = image
            })
        }
        else {
            cell.iconImage.image = event.type.image()
        }
        cell.titleLabel.text = event.title
        cell.detailLabel.text = event.dateString()
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let event = events[indexPath.row]
        
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        cell?.selected = false
        if event.type == .Festival {
            self.performSegueWithIdentifier("showFestivalList", sender: event)
        }
        else if event.type == .Concert {
            event.requestTopTracks(self)
        }
        
    }
    
    //MARK: Event delegate
    func events(events: Array<BEvent>?) {
        if let events = events {
            self.events = events
            tableView.reloadData()
        }
    }
    
    //MARK: Acts
    func actTopTracks(act: BAct) {
        BKPlayer.playSong(act)
        view.bringSubviewToFront(playerView)
        titleLabel.text = act.tracks.first?.name
    }
}
