//
//  FestivalListViewController.swift
//  Bestival
//
//  Created by Alex on 12/09/15.
//  Copyright (c) 2015 Alex Steiner. All rights reserved.
//

import UIKit
import BestivalKit

class FestivalListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, BFestivalDelegate, BActTracksDelegate {

    // IBActions
    @IBAction func playerButton(sender: AnyObject) {
        let player = BKPlayer
        player.playPause()
        if !player.isPlaying() {
            actionButton.setTitle(NSLocalizedString("Pause", comment: "Pause a song while playing"), forState: .allZeros)
        }
        else {
            actionButton.setTitle(NSLocalizedString("Play", comment: "Play a song while playing"), forState: .allZeros)
        }
    }
    
    // IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    
    // Selectors
    
    
    // Variables
    var festival:BFestival?
    
    // Main methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if let festival = festival {
            self.title = festival.title
            festival.requestActs(self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: TableView
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let festival = festival {
            return festival.acts.count
        }
        else {
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! EventListTableViewCell
        cell.detailLabel.text = nil
        if let acts = festival?.acts {
            let act = acts[indexPath.row]
            act.artistImage({
                (image:UIImage?) in
                cell.iconImage.image = image
            })
            cell.titleLabel.text = act.title
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let festival = festival {
            return festival.dateString()
        }
        else {
            return nil
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let acts = festival?.acts {
            let act = acts[indexPath.row]
            act.requestTopTracks(self)
        }
    }
    
    //MARK: Festivals
    func festival(festival: BFestival?) {
        self.festival = festival
        tableView.reloadData()
    }
    
    //MARK: Acts
    func actTopTracks(act: BAct) {
        BKPlayer.playSong(act)
        view.bringSubviewToFront(playerView)
        titleLabel.text = act.tracks.first?.name
    }
    
}
