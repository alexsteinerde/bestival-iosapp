//
//  NavigationViewController.swift
//  Bestival
//
//  Created by Alex on 12/09/15.
//  Copyright (c) 2015 Alex Steiner. All rights reserved.
//

import UIKit

class NavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.barTintColor = UIColor(red: 0.54, green: 0.76, blue: 0.29, alpha: 1)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
