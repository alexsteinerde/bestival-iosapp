//
//  Setting.swift
//  Bestival
//
//  Created by Alex on 11/09/15.
//  Copyright (c) 2015 Alex Steiner. All rights reserved.
//

import Foundation
import CoreData

class Setting: NSManagedObject {

    @NSManaged var key: String
    @NSManaged var value: String
    @NSManaged var data: NSData
    @NSManaged var boolean: NSNumber

}
