//
//  KitSettings.swift
//  Bestival
//
//  Created by Alex on 11/09/15.
//  Copyright (c) 2015 Alex Steiner. All rights reserved.
//

import Foundation

let BKCoreDataManager = CoreDataManager()
let BKServerURL = "http://178.254.31.43:9014"

extension String {
    func replaceSpecialCharacters()->String {
        var type = self.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
        type = type?.stringByReplacingOccurrencesOfString(" ", withString: "", options: .allZeros, range: nil)
        type = type?.stringByReplacingOccurrencesOfString("&", withString: "", options: .allZeros, range: nil)
        return type!
    }
}