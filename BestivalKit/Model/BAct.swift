//
//  BAct.swift
//  Bestival
//
//  Created by Alex on 12/09/15.
//  Copyright (c) 2015 Alex Steiner. All rights reserved.
//

import Foundation

public protocol BActTracksDelegate {
    func actTopTracks(act:BAct)
}

public class BAct : BConnectionDelegate {
    public var title:String
    private var requestArtistImage:((image:UIImage?)->Void)?
    public var artistImage:UIImage?
    private var topTracksDelegate:BActTracksDelegate?
    public var tracks = Array<BTrack>()
    
    public init(title:String) {
        self.title = title
    }
    
    public func artistImage(completion:(image:UIImage?)->Void) {
        if let artistImage = artistImage {
            completion(image: artistImage)
        }
        else {
            self.requestArtistImage = completion
            BConnection(delegate: self, id: 0).apiRequest("pics/artists?artist=\(self.titleWithOutType(type: .Concert))", params: nil, method: "GET")
        }
    }
    
    public func requestTopTracks(delegate:BActTracksDelegate) {
        self.topTracksDelegate = delegate
        BConnection(delegate: self, id: 2).apiRequest("tracks?artist=\(self.titleWithOutType(type: .Concert))", params: nil, method: "GET")
    }
    
    public func titleWithOutType(type:BEventType=BEventType.Concert)->String {
        return title.stringByReplacingOccurrencesOfString(type.typeString(), withString: "", options: .allZeros, range: nil)
    }
    
    //MARK: Connection
    func connection(connection: BConnection, didFailWithError: NSError) {
    }
    
    func connection(connection: BConnection, didFinishRequest data: BResponseData) {
        if connection.connectionId == 0 {
             if let objects = data.jsonDic as? Dictionary<String, String>, let imageUrl = objects["artist_image"] where data.errorCode == 201 {
                let image = BConnection.downloadImage(imageUrl)
                if let completion = requestArtistImage {
                    self.artistImage = image
                    completion(image: image)
                }
                requestArtistImage = nil
            }
        }
        else if connection.connectionId == 2 {
            if let objects = data.jsonDic as? Array<Dictionary<String, String>> where data.errorCode == 201 {
                self.tracks = []
                for object in objects {
                    if let uri = object["uri"], name = object["name"] {
                        self.tracks += [BTrack(uri: uri, name: name)]
                    }
                }
            }
            if let delegate = topTracksDelegate {
                delegate.actTopTracks(self)
            }
        }
    }
}