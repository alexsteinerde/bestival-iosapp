//
//  BConnection.swift
//  Bestival
//
//  Created by Alex on 12/09/15.
//  Copyright (c) 2015 Alex Steiner. All rights reserved.
//

import Foundation
import UIKit

/**
*  Protocol for handling the connection
*/
protocol BConnectionDelegate {
    /**
    Handle data after got them from the Api
    
    :param: connection BConnection
    :param: data       BResponseData with the requested data
    */
    func connection(connection:BConnection, didFinishRequest data:BResponseData)
    
    /**
    Handle any errors which appeard during the request
    
    :param: connection       BConnection
    :param: didFailWithError NSError with the exactly error
    */
    func connection(connection:BConnection, didFailWithError:NSError)
}

class BConnection:NSObject, NSURLConnectionDataDelegate {
    // Variables
    private var serverURL = BKServerURL
    private var data:NSMutableData
    private var delegate:BConnectionDelegate?
    private var apiRequest = true
    private var totalFileSize = Int()
    private var receivedDataBytes = Int()
    var connectionId:Int
    private var connection:NSURLConnection?
    private var request:NSMutableURLRequest?
    var statusCode:Int?
    
    init(delegate:BConnectionDelegate?=nil, id:Int=0) {
        self.delegate = delegate
        self.data = NSMutableData()
        self.connectionId = id
        receivedDataBytes = 0
    }
    /**
    Does a request to the API.
    
    :param: type              Where do you want to do the request?
    :param: params            What do you want to give to the server?
    :param: method            Which method do you want to use? GET, POST?
    :param: autorizationNeeds Should it use the token for autorization?
    */
    internal func apiRequest(type:String, params:Dictionary<String,Any>?, method:String="POST"){
        var request = NSMutableURLRequest(URL: NSURL(string: "\(serverURL)/\(type.replaceSpecialCharacters())")!)
        request.cachePolicy = .ReloadIgnoringLocalCacheData
        request.HTTPMethod = method
        request.timeoutInterval = 300
        request.HTTPShouldUsePipelining = true
        
        var paramList = Array<String>()
        if let params = params {
            for (key,value) in params {
                paramList += ["\(key)=\(value)"]
            }
        }
        var param = String()
        for var i = 0; i < paramList.count; i++ {
            if i > 0 {
                param += "&"
            }
            param += paramList[i]
        }
        var data = param.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true) // default: true
        if let paramData = data {
            request.HTTPBody = paramData
            request.addValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.addValue("\(paramData.length)", forHTTPHeaderField: "Content-Length")
            self.request = request
            startConnection()
        }
    }
    
    /**
    Start the connection which was defined earlier
    */
    private func startConnection() {
        if let request = request {
            self.connection = NSURLConnection(request: request, delegate: self)
            if let connection = self.connection {
                connection.start()
            }
            else if self.connection == nil {
                if let delegate = self.delegate {
                    delegate.connection(self, didFinishRequest: BResponseData(errorCode: 999, data: nil, nsdata:nil))
                }
            }
        }
    }
    
    /**
    Decode the data to json
    
    :param: data data from the API request
    
    :returns: HBResponsedata with the json object and a status code
    */
    private func dataToJson(data:NSData)->BResponseData {
        var dataResponse = BResponseData()
        
        var jsonObject: AnyObject? = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: nil)
        if let jsonReturn: AnyObject = jsonObject {
            dataResponse.errorCode = 200
            dataResponse.jsonDic = jsonReturn
            return BResponseData(errorCode: 201, data: jsonReturn, nsdata: nil)
        }
        dataResponse.errorCode = 998
        return dataResponse
    }
    
    class func downloadImage(url:String)->UIImage? {
        if let url = NSURL(string: url) {
            if let data = NSData(contentsOfURL: url) {
                return UIImage(data: data)
            }
        }
        return nil
    }
    
    //MARK: Handle the NSURLConnection stuff
    func connection(connection: NSURLConnection, didFailWithError error: NSError) {
        println(error)
        if let delegate = self.delegate {
            delegate.connection(self, didFinishRequest: BResponseData(errorCode: 999, data: nil, nsdata:nil))
        }
        if let delegate = delegate {
            delegate.connection(self, didFailWithError: error)
        }
    }
    
    func connection(connection: NSURLConnection, didReceiveResponse response: NSURLResponse) {
        data.length = 0
        totalFileSize = Int(response.expectedContentLength)
        if let response:NSHTTPURLResponse = response as? NSHTTPURLResponse {
            self.statusCode = response.statusCode
        }
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData) {
        self.data.appendData(data)
        receivedDataBytes += data.length
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection) {
        if apiRequest {
            if let delegate = self.delegate {
                delegate.connection(self, didFinishRequest: dataToJson(data))
            }
        }
        else {
            if let delegate = self.delegate {
                delegate.connection(self, didFinishRequest: BResponseData(errorCode: 200, data: nil, nsdata: data))
            }
        }
    }
}

/**
*  Handle returned data from the API
*/
struct BResponseData {
    init() {
        errorCode = 0
    }
    
    init(errorCode:Int, data:AnyObject?, nsdata:NSData?) {
        self.errorCode = errorCode
        self.jsonDic = data
        self.data = nsdata
    }
    
    var errorCode:Int
    var jsonDic:AnyObject?
    var data:NSData?
}
