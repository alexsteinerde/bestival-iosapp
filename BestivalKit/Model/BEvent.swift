//
//  BEvent.swift
//  Bestival
//
//  Created by Alex on 12/09/15.
//  Copyright (c) 2015 Alex Steiner. All rights reserved.
//

import Foundation
import EventKit

public protocol BEventDelegate {
    func events(events:Array<BEvent>?)
}

public enum BEventType {
    case Festival
    case Concert
    
    func typeString()->String {
        switch self {
        case .Festival:
            return NSLocalizedString("Festival", comment: "Key for event searching")
        case .Concert:
            return NSLocalizedString("Concert", comment: "Key for event searching")
        default:
            return ""
        }
    }
    
    public func image()->UIImage? {
        switch self {
        case .Festival:
            return UIImage(named: "Festival")
        case .Concert:
            return UIImage(named: "Concert")
        default:
            return UIImage()
        }
    }
}

public class BEvent :  BAct {
    
    public var date:NSDate
    var calendarIdentifier:String
    public var type:BEventType
    
    init(title:String, date:NSDate, calendarIdentifier:String, type:BEventType) {
        self.type = type
        self.calendarIdentifier = calendarIdentifier
        self.date = date
        super.init(title: "Test")
        self.title = title
    }
    
    public func dateString()->String {
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateStyle = .MediumStyle
        dateFormatter.timeStyle = .NoStyle
        
        return dateFormatter.stringFromDate(self.date)
    }
    
    class func readEvents(completion:(events:Array<EKEvent>?)->Void) {
        let eventStore = EKEventStore()
        eventStore.requestAccessToEntityType(EKEntityTypeEvent, completion: {
            (granted, error) in
            let calendarArray = eventStore.calendarsForEntityType(EKEntityTypeEvent)
            if let calendars = calendarArray as? Array<EKCalendar> {
                BEvent.eventsForCalendars(eventStore, calendars: calendars, completion: completion)
            }
            else {
                completion(events: nil)
            }
        })
    }
    
    class func eventsForCalendars(eventStore:EKEventStore, calendars:Array<EKCalendar>, completion:(events:Array<EKEvent>?)->Void) {
        let startDate = NSDate(timeIntervalSinceNow: -86400*366)
        let endDate = NSDate(timeIntervalSinceNow: 86400 * 366)
        let fetchCalendarEvents = eventStore.predicateForEventsWithStartDate(startDate, endDate: endDate, calendars: calendars)
        let eventList = eventStore.eventsMatchingPredicate(fetchCalendarEvents)
        if let events = eventList as? Array<EKEvent> {
            completion(events:events)
        }
    }
    
    public class func requestSortedEvents(delegate:BEventDelegate) {
        BEvent.readEvents({
            (events) in
            if let events = events {
                var allEvents = Array<BEvent>()
                for event in events {
                    if let eventType = BEvent.decideFestivalConcert(event.title) {
                        var newEvent:BEvent
                        switch eventType {
                        case .Festival:
                            newEvent = BFestival(title: event.title, date: event.startDate, calendarIdentifier: event.eventIdentifier, type: eventType)
                        default:
                            newEvent = BEvent(title: event.title, date: event.startDate, calendarIdentifier: event.eventIdentifier, type: eventType)
                        }
                        allEvents += [newEvent]
                    }
                }
                delegate.events(allEvents)
            }
            else {
                delegate.events(nil)
            }
        })
    }
    
    class func decideFestivalConcert(title:String)->BEventType? {
        let titleSpit = title.componentsSeparatedByString(" ")
        if let lastString = titleSpit.last {
            switch lastString {
            case NSLocalizedString("Festival", comment: "Key for event searching"):
                return .Festival
            case NSLocalizedString("Concert", comment: "Key for event searching"):
                return .Concert
            default:
                return nil
            }
        }
        else {
            return nil
        }
    }
}