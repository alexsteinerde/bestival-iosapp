//
//  BFestival.swift
//  Bestival
//
//  Created by Alex on 12/09/15.
//  Copyright (c) 2015 Alex Steiner. All rights reserved.
//

import Foundation

public protocol BFestivalDelegate {
    func festival(festival:BFestival?)
}

public class BFestival : BEvent, BConnectionDelegate {
    
    public var acts = Array<BAct>()
    public var year:Int {
        get {
            let components = NSCalendar.currentCalendar().components(NSCalendarUnit.CalendarUnitYear, fromDate: self.date)
            return components.year
        }
    }
    var delegate:BFestivalDelegate?
    
    public func requestActs(delegate:BFestivalDelegate) {
        self.delegate = delegate
        BConnection(delegate: self, id: 1).apiRequest("lineups", params: ["name":self.titleWithOutType(type:self.type), "year":self.year], method: "POST")
    }
    
    
    //MARK: Connection
    override func connection(connection: BConnection, didFailWithError: NSError) {
        super.connection(connection, didFailWithError: didFailWithError)
        if connection.connectionId == 1 {
            if let delegate = delegate {
                delegate.festival(nil)
            }
        }

    }
    
    override func connection(connection: BConnection, didFinishRequest data: BResponseData) {
        super.connection(connection, didFinishRequest: data)
        if connection.connectionId == 1 {
            if let objects = data.jsonDic as? Dictionary<String, AnyObject>, festivalName = objects["festival"] as? String, acts = objects["acts"] as? Array<String> where connection.statusCode == 200 {
                for act in acts {
                    self.acts += [BAct(title: act)]
                }
                if let delegate = delegate {
                    delegate.festival(self)
                }
            }
        }
        
    }
}