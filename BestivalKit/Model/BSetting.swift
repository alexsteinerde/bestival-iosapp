//
//  BSetting.swift
//  Bestival
//
//  Created by Alex on 11/09/15.
//  Copyright (c) 2015 Alex Steiner. All rights reserved.
//

import Foundation
import CoreData
import UIKit

/// Handle settings with this class. It will save everything into the local CoreData database
public class BSetting {
    
    public class func setValue(value:String, forKey key:String) {
        let context = BKCoreDataManager.managedObjectContext!
        var fetch = NSFetchRequest()
        fetch.entity = NSEntityDescription.entityForName("Setting", inManagedObjectContext: context)
        fetch.fetchLimit = 1
        fetch.returnsObjectsAsFaults = false
        fetch.predicate = NSPredicate(format: "key == %@", argumentArray: [key])
        
        if context.countForFetchRequest(fetch, error: nil) == 0 {
            var entity:Setting = NSEntityDescription.insertNewObjectForEntityForName("Setting", inManagedObjectContext: context) as! Setting
            entity.key = key
            entity.value = value
        }
        else {
            var objects:Array = context.executeFetchRequest(fetch, error: nil)!
            
            for object:Setting in objects as! Array {
                object.value = value
            }
            
        }
        context.save(nil)
    }
    
    public class func value(key:String)->String? {
        let context = BKCoreDataManager.managedObjectContext!
        var fetch = NSFetchRequest()
        fetch.entity = NSEntityDescription.entityForName("Setting", inManagedObjectContext: context)
        fetch.fetchLimit = 1
        fetch.returnsObjectsAsFaults = false
        fetch.predicate = NSPredicate(format: "key == %@", argumentArray: [key])
        
        var objects:Array = context.executeFetchRequest(fetch, error: nil)!
        if objects.count > 0 {
            var object:Setting = objects.first as! Setting
            return object.value
        }
        else {
            return nil
        }
    }
    
    public class func setB(boolean:Bool, forKey key:String) {
        let context = BKCoreDataManager.managedObjectContext!
        var fetch = NSFetchRequest()
        fetch.entity = NSEntityDescription.entityForName("Setting", inManagedObjectContext: context)
        fetch.fetchLimit = 1
        fetch.returnsObjectsAsFaults = false
        fetch.predicate = NSPredicate(format: "key == %@", argumentArray: [key])
        
        if context.countForFetchRequest(fetch, error: nil) == 0 {
            var entity:Setting = NSEntityDescription.insertNewObjectForEntityForName("Setting", inManagedObjectContext: context) as! Setting
            entity.key = key
            entity.boolean = NSNumber(bool: boolean)
        }
        else {
            var objects:Array = context.executeFetchRequest(fetch, error: nil)!
            
            for object:Setting in objects as! Array {
                object.boolean = NSNumber(bool: boolean)
            }
            
        }
        context.save(nil)
    }
    
    public class func boolean(key:String)->Bool? {
        let context = BKCoreDataManager.managedObjectContext!
        var fetch = NSFetchRequest()
        fetch.entity = NSEntityDescription.entityForName("Setting", inManagedObjectContext: context)
        fetch.fetchLimit = 1
        fetch.returnsObjectsAsFaults = false
        fetch.predicate = NSPredicate(format: "key == %@", argumentArray: [key])
        
        var objects:Array = context.executeFetchRequest(fetch, error: nil)!
        if objects.count > 0 {
            var object:Setting = objects.first as! Setting
            return object.boolean.boolValue
        }
        else {
            return nil
        }
    }
    
    public class func setData(value:NSData, forKey key:String) {
        let context = BKCoreDataManager.managedObjectContext!
        var fetch = NSFetchRequest()
        fetch.entity = NSEntityDescription.entityForName("Setting", inManagedObjectContext: context)
        fetch.fetchLimit = 1
        fetch.returnsObjectsAsFaults = false
        fetch.predicate = NSPredicate(format: "key == %@", argumentArray: [key])
        
        if context.countForFetchRequest(fetch, error: nil) == 0 {
            var entity:Setting = NSEntityDescription.insertNewObjectForEntityForName("Setting", inManagedObjectContext: context) as! Setting
            entity.key = key
            entity.data = value
        }
        else {
            var objects:Array = context.executeFetchRequest(fetch, error: nil)!
            
            for object:Setting in objects as! Array {
                object.data = value
            }
            
        }
        context.save(nil)
    }
    
    public class func data(key:String)->NSData? {
        let context = BKCoreDataManager.managedObjectContext!
        var fetch = NSFetchRequest()
        fetch.entity = NSEntityDescription.entityForName("Setting", inManagedObjectContext: context)
        fetch.fetchLimit = 1
        fetch.returnsObjectsAsFaults = false
        fetch.predicate = NSPredicate(format: "key == %@", argumentArray: [key])
        
        var objects:Array = context.executeFetchRequest(fetch, error: nil)!
        if objects.count > 0 {
            var object:Setting = objects.first as! Setting
            return object.data
        }
        else {
            return nil
        }
    }
    
    class func uniqueID()->String {
        if let uuid = BSetting.value("MMKDeviceUUID") {
            return uuid
        }
        else {
            let uuid = UIDevice.currentDevice().identifierForVendor.UUIDString
            BSetting.setValue(uuid, forKey: "MMKDeviceUUID")
            return uuid
        }
    }
}