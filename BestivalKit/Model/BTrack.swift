//
//  BTrack.swift
//  Bestival
//
//  Created by Alex on 12/09/15.
//  Copyright (c) 2015 Alex Steiner. All rights reserved.
//

public class BTrack {
    public let uri:String
    public let name:String
    
    init(uri:String, name:String) {
        self.uri = uri
        self.name = name
    }
}
