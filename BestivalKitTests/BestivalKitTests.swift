//
//  BestivalKitTests.swift
//  BestivalKitTests
//
//  Created by Alex on 11/09/15.
//  Copyright (c) 2015 Alex Steiner. All rights reserved.
//

import UIKit
import XCTest
import BestivalKit

class BestivalKitTests: XCTestCase, BEventDelegate {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        BEvent.requestSortedEvents(self)
        let act = BAct(title:"K.I.Z Concert")
        
        XCTAssert(act.titleWithOutType(type: .Concert) == "K.I.Z ", "String replacement")
        
        println(act)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        XCTAssert(true, "Pass")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            
        }
    }
    
    //MARK: Event
    func events(events: Array<BEvent>?) {
        XCTAssertTrue(events?.count > 0 , "Got events")
    }
    
}
