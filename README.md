This is the iOS application for the project Bestival. Bestival searches for festivals and concerts in your calendar and connects this with the acts and artists there. You have the chance to play songs from spotify by these artists.
The application uses the newest software technology, native iOS programming and the newest version of #Swift. The infrastructure is devided into a few parts. The main part manages all the connections to the server and searches for events in the calendar. This framework has been designed very carefully for having the best and easiest chance to use this also in other projects by creating a completly restricted area.
Then, there is the iPhone app, which represent the UI and main storyboard of the app. 
For the best native feeling in the Apple ecosystem, you can also use the native Watch application that gets all of its data from the framework.
Use Bestival.xcodeproj for the open project.

Used librarys:
- Spotify, under the Apache license down under

Apache License
Version 2.0, January 2004
http://www.apache.org/licenses/